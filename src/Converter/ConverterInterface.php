<?php

namespace App\Converter;

interface ConverterInterface {
    
    public function convert($_field);
}
