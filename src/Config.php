<?php

namespace App;

class Config {
    const API_ENDPOINT = 'http://api.bonotel.com/index.cfm/user/voyagrs_xml/action/hotel';
    const DATA_DIRECTORY = __DIR__ . '/../data/';
    const DATA_FILE_PREFIX = 'HS_BNO_H_';
    const DISTRIBUTOR = 'BONOTEL';
}
