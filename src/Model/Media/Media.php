<?php

namespace App\Model\Media;

use App\Model\Media\MediaWeight;
use App\Model\Media\MediaSize;

class Media {
    /**
     *
     * @var MediaWeight weight
     */
    private $m_weight;
    
    /**
     *
     * @var MediaSize size
     */
    private $m_size;
    
    /**
     *
     * @var string
     */
    private $m_url;
    
    public function getM_weight() {
        return $this->m_weight;
    }

    public function getM_size() {
        return $this->m_size;
    }

    public function getM_url() {
        return $this->m_url;
    }

    public function setM_weight(MediaWeight $_weight) {
        $this->m_weight = $_weight;
    }

    public function setM_size(MediaSize $_size) {
        $this->m_size = $_size;
    }

    public function setM_url($_url) {
        $this->m_url = $_url;
    }
    
    public function serialize() {
        return [
               'size' => $this->m_size ? $this->m_size->serialize() : null,
                'weight' => $this->m_weight ? $this->m_weight->serialize() : null,
                'url' => $this->m_url
        ];
    }
}