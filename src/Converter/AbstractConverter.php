<?php

namespace App\Converter;

use App\Converter\Extractor\FieldExtractor;

abstract class AbstractConverter implements ConverterInterface {
    
    /**
     * @var FieldExtractor
     */
    protected $m_extractor;
    
    /**
     * @param string $node
     */
    public function __construct($node = null) {
        $this->m_extractor = new FieldExtractor();
        if ($node) {
            $this->m_extractor->setM_node(new \SimpleXMLElement($node, LIBXML_NOCDATA));
        }
    }
    
    public function getM_extractor(): FieldExtractor {
        return $this->m_extractor;
    }

    public function setM_extractor(FieldExtractor $_extractor) {
        $this->m_extractor = $_extractor;
    }

    protected function checkData() {
        if (!$this->getM_extractor()->getM_node()) {
            throw new ConvertException();
        }
    }
}

