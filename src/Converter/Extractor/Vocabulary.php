<?php

namespace App\Converter\Extractor;

class Vocabulary {

    const EN = [
        'swimmingpool' => ['swim', 'pool', 'pools', 'swimmingpool'],
        'parking' => ['parking'],
        'fitness' => ['workout', 'gym', 'fitness'],
        'golf' => ['golf'],
        'seaside' => ['seaside'],
        'spa' => ['spa', 'water spring', 'sauna', 'jacuzzi'],
        'ecotourism' => ['natural'],
        'familyFriendly' => ['family'],
        'pmr' => ['handicap'],
        'wedding' => ['wedding']
    ];

    const FR = [];
    
    /**
     * 
     * @param string $_countryCode
     * @return array
     */
    public static function loadVocabulary($_countryCode) {
        switch ($_countryCode) {
            case 'FR':
                return self::FR;
            default :
                return self::EN;
        }
    }
}
