<?php
namespace Tests\App\Converter;

use PHPUnit\Framework\TestCase;
use App\Converter\DistributionConverter;

class DistributionConverterTest extends TestCase {
    
    private $m_nodeString = ''
            . '<hotel>'
            .'<hotelCode>1024</hotelCode>'
            . '</hotel>';

    public function testExtract() {
        $res =  (new DistributionConverter($this->m_nodeString))->convert();
        $this->assertSame(['BONOTEL' => '1024'], $res);
    }
}
