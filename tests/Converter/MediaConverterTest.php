<?php
namespace Tests\App\Converter;

use PHPUnit\Framework\TestCase;
use App\Converter\MediaConverter;
use App\Model\Media\Media;

class MediaConverterTest extends TestCase {
    
    private $m_nodeString = ''
            . '<hotel>'
            . '<images>'
                . '<image>http://legacy.bonotel.com/photos/22791.jpg</image>'
                . '<image>http://legacy.bonotel.com/photos/3094.jpg</image>'
            . '</images>'
            . '</hotel>';

    public function testExtract() {
        $res = (new MediaConverter($this->m_nodeString))->convert();
        $this->assertInstanceOf(Media::class, $res);
        $this->assertEquals('http://legacy.bonotel.com/photos/22791.jpg', $res->getM_url());
        
        $this->assertNotEmpty($res->getM_size());
        $this->assertNotEmpty($res->getM_size()->getM_height());
        $this->assertNotEmpty($res->getM_size()->getM_width());
        $this->assertEquals('px', $res->getM_size()->getM_unit());
        
        $this->assertNotEmpty($res->getM_weight());
        $this->assertIsInt($res->getM_weight()->getM_value());
        $this->assertEquals('Byte', $res->getM_weight()->getM_unit());
    }
}