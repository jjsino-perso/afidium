<?php

namespace App\Model\Media;

class MediaSize {
    /**
     * @var int width
     */
    private $m_width;
    
    /**
     * @var int height
     */
    private $m_height;
    
    /**
     * @var string unit
     */
    private $m_unit;
    
    function __construct($_width, $_height, $_unit = 'px') {
        $this->m_width = $_width;
        $this->m_height = $_height;
        $this->m_unit = $_unit;
    }

    public function getM_width() {
        return $this->m_width;
    }

    public function getM_height() {
        return $this->m_height;
    }

    public function getM_unit() {
        return $this->m_unit;
    }

    public function setM_width($_width) {
        $this->m_width = $_width;
    }

    public function setM_height($_height) {
        $this->m_height = $_height;
    }

    public function setM_unit($_unit) {
        $this->m_unit = $_unit;
    }
    
    public function serialize() {
        return [
            'height' => $this->m_height,
            'width' => $this->m_width,
            'unit' => $this->m_unit
        ];
    }
}