<?php

namespace App\Converter;

class FloatFieldConverter extends AbstractConverter implements ConverterInterface {
    
    /**
     * @param string $_field
     * @return float data 
     */
    public function convert($_field) {
        $str = $this->m_extractor->getFirstNodeData($_field);
        return empty($str) ? null : floatval($str);
    }
}