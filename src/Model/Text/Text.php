<?php

namespace App\Model\Text;

class Text {

    /**
     * @var  string language
     */
    private $m_language;

    /**
     * @var  string type
     */
    private $m_typeCode;

    /**
     * @var  string title
     */
    private $m_title;

    /**
     * @var  string text
     */
    private $m_text;
    
    function __construct($_language, $_typeCode, $_title, $_text) {
        $this->m_language = $_language;
        $this->m_typeCode = $_typeCode;
        $this->m_title = $_title;
        $this->m_text = $_text;
    }

    public function getM_language() {
        return $this->m_language;
    }

    public function getM_typeCode() {
        return $this->m_typeCode;
    }

    public function getM_title() {
        return $this->m_title;
    }

    public function getM_text() {
        return $this->m_text;
    }

    public function setM_language($_language) {
        $this->m_language = $_language;
    }

    public function setM_typeCode($_typeCode) {
        $this->m_typeCode = $_typeCode;
    }

    public function setM_title($_title) {
        $this->m_title = $_title;
    }

    public function setM_text($_text) {
        $this->m_text = $_text;
    }
    
    public function serialize() {
        return [
               'language' => $this->m_language,
                'type_code' => $this->m_typeCode,
                'title' => $this->m_title,
                'text' => $this->m_text
        ];
    }
}
