<?php

namespace Tests\App\Converter;

use PHPUnit\Framework\TestCase;
use App\Converter\FloatFieldConverter;

class FloatFieldConverterTest extends TestCase {
    
    private $m_nodeString = ''
            . '<hotel>'
            .'<latitude>1024</latitude>'
            .'<longitude></longitude>'
            . '</hotel>';

    public function testExtract() {
        $this->assertSame(1024.0, (new FloatFieldConverter($this->m_nodeString))->convert('latitude'));
    }
    
    public function testExtractEmpty() {
        $this->assertNull((new FloatFieldConverter($this->m_nodeString))->convert('longitude'));
    }
}
