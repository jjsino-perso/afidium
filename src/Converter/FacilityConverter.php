<?php

namespace App\Converter;

use App\Converter\Extractor\FieldExtractor;
use App\Converter\Extractor\Vocabulary;

class FacilityConverter extends AbstractConverter implements ConverterInterface {
    
    const XML_NODES = ['recreation', 'facilities'];
    
    /**
     * @param string $_field
     * @return bool data 
     */
    public function convert($_field) {
        $countryCode = $this->m_extractor->getFirstNodeData('countryCode');
        
        if (!isset(Vocabulary::loadVocabulary($countryCode)[$_field])) {
            return FALSE;
        }
        
        $keywords = Vocabulary::loadVocabulary($countryCode)[$_field];
        
        foreach (self::XML_NODES as $nodeName) {
            $content = $this->m_extractor->getFirstNodeData($nodeName);
            
            foreach ($keywords as $keyword) {
                if (stripos($content, $keyword) !== FALSE) {
                    return TRUE;
                } 
            }
        }
        
        return FALSE;
    }
}
