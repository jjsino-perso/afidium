<?php

namespace App\Model;

use App\Model\Text\Text;
use App\Model\Media\Media;

class Hotel {
	/**
	 * @var float latitude
	 */
	private $m_latitude;

	/**
	 * @var float longitude
	 */
	private $m_longitude;

	/**
	 * @var string 
	 */
	private $m_language;

	/**
	 * @var float rating_level
	 */
	private $m_ratingLevel;

	/**
	 * @var bool swimming pool
	 */
	private $m_swimmingpool;

	/**
	 * @var bool parking
	 */
	private $m_parking;

	/**
	 * @var bool fitness
	 */
	private $m_fitness;

	/**
	 * @var bool golf
	 */
	private $m_golf;

	/**
	 * @var bool seaside
	 */
	private $m_seaside;

	/**
	 * @var bool spa
	 */
	private $m_spa;

	/**
	 * @var bool charm
	 */
	private $m_charm;
        
	/**
	 * @var bool ecotourism
	 */
	private $m_ecotourism;

	/**
	 * @var bool exceptional
	 */
        private $m_exceptional;
	/**
	 * @var bool family friendly
	 */
	private $m_familyFriendly;

	/**
	 * @var bool pmr
	 */
	private $m_pmr;

	/**
	 * @var bool preferred
	 */
	private $m_preferred;

	/**
	 * @var bool wedding
	 */
	private $m_wedding;

        /**
         * @var array distribution
         */
	private $m_distribution;

        /**
         *
         * @var Text
         */
	private $m_introductionText;

        /**
         *
         * @var Media
         */
	private $m_introductionMedia;
        
        public function getM_latitude() {
            return $this->m_latitude;
        }

        public function getM_longitude() {
            return $this->m_longitude;
        }

        public function getM_language() {
            return $this->m_language;
        }

        public function getM_ratingLevel() {
            return $this->m_ratingLevel;
        }

        public function getM_swimmingpool() {
            return $this->m_swimmingpool;
        }

        public function getM_parking() {
            return $this->m_parking;
        }

        public function getM_fitness() {
            return $this->m_fitness;
        }

        public function getM_golf() {
            return $this->m_golf;
        }

        public function getM_seaside() {
            return $this->m_seaside;
        }

        public function getM_spa() {
            return $this->m_spa;
        }

        public function getM_ecotourism() {
            return $this->m_ecotourism;
        }

        public function getM_familyFriendly() {
            return $this->m_familyFriendly;
        }

        public function getM_pmr() {
            return $this->m_pmr;
        }

        public function getM_preferred() {
            return $this->m_preferred;
        }

        public function getM_wedding() {
            return $this->m_wedding;
        }

        public function getM_distribution() {
            return $this->m_distribution;
        }

        public function getM_introductionText() {
            return $this->m_introductionText;
        }

        public function getM_introductionMedia() {
            return $this->m_introductionMedia;
        }

        public function setM_latitude($_latitude) {
            $this->m_latitude = $_latitude;
        }

        public function setM_longitude($_longtitude) {
            $this->m_longitude = $_longtitude;
        }

        public function setM_language($_language) {
            $this->m_language = $_language;
        }

        public function setM_ratingLevel($_ratingLevel) {
            $this->m_ratingLevel = $_ratingLevel;
        }

        public function setM_swimmingpool($_swimmingpool) {
            $this->m_swimmingpool = $_swimmingpool;
        }

        public function setM_parking($_parking) {
            $this->m_parking = $_parking;
        }

        public function setM_fitness($_fitness) {
            $this->m_fitness = $_fitness;
        }

        public function setM_golf($_golf) {
            $this->m_golf = $_golf;
        }

        public function setM_seaside($_seaside) {
            $this->m_seaside = $_seaside;
        }

        public function setM_spa($_spa) {
            $this->m_spa = $_spa;
        }

        public function setM_ecotourism($_ecotourism) {
            $this->m_ecotourism = $_ecotourism;
        }

        public function setM_familyFriendly($_familyFriendly) {
            $this->m_familyFriendly = $_familyFriendly;
        }

        public function setM_pmr($_pmr) {
            $this->m_pmr = $_pmr;
        }

        public function setM_preferred($_preferred) {
            $this->m_preferred = $_preferred;
        }

        public function setM_wedding($_wedding) {
            $this->m_wedding = $_wedding;
        }

        public function setM_distribution($_distribution) {
            $this->m_distribution = $_distribution;
        }

        public function setM_introductionText($_introductionText) {
            $this->m_introductionText = $_introductionText;
        }

        public function setM_introductionMedia($_introductionMedia) {
            $this->m_introductionMedia = $_introductionMedia;
        }
        public function getM_exceptional() {
            return $this->m_exceptional;
        }

        public function setM_exceptional($_exceptional) {
            $this->m_exceptional = $_exceptional;
        }
        public function getM_charm() {
            return $this->m_charm;
        }

        public function setM_charm($_charm) {
            $this->m_charm = $_charm;
        }

        public function serialize() {
            return [
                'latitude' => $this->m_latitude ? $this->m_latitude : null,
                'longitude' => $this->m_longitude ? $this->m_longitude : null,
                'language' => $this->m_language,
                'rating_level' => $this->m_ratingLevel ? $this->m_ratingLevel : null,
                'swimmingpool' => $this->m_swimmingpool,
                'parking' => $this->m_parking,
                'fitness' => $this->m_fitness,
                'golf' => $this->m_golf,
                'seaside' => $this->m_seaside,
                'spa' => $this->m_spa,
                'charm' => $this->m_charm,
                'ecotourism' => $this->m_ecotourism,
                'exceptional' => $this->m_exceptional,
                'family_friendly' => $this->m_familyFriendly,
                'pmr' => $this->m_pmr,
                'preferred' => $this->m_preferred,
                'wedding' => $this->m_wedding,
                'distribution' => $this->m_distribution,
                'introduction_text' => $this->m_introductionText ? $this->m_introductionText->serialize() : null,
                'introduction_media' => $this->m_introductionMedia ? $this->m_introductionMedia->serialize() : null
            ];
        }
}