<?php

namespace App;

use GuzzleHttp\Client;
use App\Config;
use App\Converter\HotelConverter;

class App {

    /**
     *
     * @var HotelConverter converter
     */
    private $m_converter;
    
    public function run() {
        
        // step 1: get response
        $client = new Client();
        $response = $client->get(Config::API_ENDPOINT);
        $content = (string) $response->getBody();
        file_put_contents(Config::DATA_DIRECTORY.Config::DATA_FILE_PREFIX.'.xml', $content);
        $xml = simplexml_load_string($content);
        
        $this->m_converter = new HotelConverter();
        
        foreach ($xml->children() as $child) {
            
               // step 2: Convert data to Object
               $this->m_converter->setM_data($child);
               $hotel = $this->m_converter->convert();
               $distribution = $hotel->getM_distribution();
               
               // step 3: Convert object to json
               $fp    = fopen(
                       Config::DATA_DIRECTORY
                       .Config::DATA_FILE_PREFIX
                       .$distribution[Config::DISTRIBUTOR]
                       .'.json', 'w');
               fwrite($fp, json_encode($hotel->serialize()));
               fclose($fp);

        }
        
    }

}
