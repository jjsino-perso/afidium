<?php

namespace Tests\App\Converter\Extractor;

use PHPUnit\Framework\TestCase;
use App\Converter\Extractor\FieldExtractor;

class FieldExtractorTest extends TestCase {
    
    private $m_nodeString = ''
            . '<hotel>'
            .'<name>random name</name>'
            . '</hotel>';

    public function testExtract() {
        $node = new \SimpleXMLElement($this->m_nodeString);
        $res =  (new FieldExtractor($node))->getFirstNodeData('name');
        $this->assertSame('random name', $res);
    }
}
