<?php

namespace App\Converter;

use App\Model\Media\Media;
use App\Model\Media\MediaSize;
use App\Model\Media\MediaWeight;

class MediaConverter extends AbstractConverter implements ConverterInterface {
    
    const PATH = 'images/image';
    const URL_PATTERN = "/.(jpe?g|png|gif|svg)$/i";
    /**
     * @param SimpleXMLElement $node
     * @param string $_field
     * @return Meida data 
     */
    public function convert($_field = null) {
        $data = $this->m_extractor->getQueryData(self::PATH);
        
        if (!$data || count($data) === 0) {
            return null;
        }
        
        $url = $this->findFirstImageUrl($data);
        if ($url === null) {
            return null;
        }
        
        $media = new Media();
        $media->setM_url($url);
        
        $size = $this->getImageSize($url);
        if ($size) {
            $media->setM_size($size);
        }
        
        $weight = $this->getImageWeight($url);
        if ($weight) {
            $media->setM_weight($weight);
        }
        
        return $media;
    }
    
    /**
     * @param array $data
     * @return string url
     */
    private function findFirstImageUrl($data = []) {
        
        foreach($data as $url) {
            $arr = explode("?", $url);
            if (preg_match(self::URL_PATTERN, $arr[0])) {
                return $url;
            }
        }
        
        return null;
    }
    
    /**
     * @param string $url
     * @return MediaSize
     */
    private function getImageSize($url = null) {
        list($width, $height, , ) = getimagesize($url);
        return new MediaSize($width, $height);
    }
    
    /**
     * 
     * @param type $url
     * @return MediaWeight
     */
    private function getImageWeight($url = null) {
        $info = exif_read_data($url, 0, TRUE);
        
        if ($info && isset($info['FILE']) && isset($info['FILE']['FileSize'])) {
            return new MediaWeight(intval($info['FILE']['FileSize']));
        }
        
        return null;
    }
}