<?php

namespace App\Converter;

class FieldConverter extends AbstractConverter implements ConverterInterface {
    
    public function convert($_field) {
        $content = $this->m_extractor->getFirstNodeData($_field);
        return trim($content);
    }

}