<?php

namespace App\Model\Media;

class MediaWeight {
    /**
     * @var int value
     */
    private $m_value;
    
    /**
     * @var string unit
     */
    private $m_unit;
    
    public function __construct($_value, $_unit = 'Byte') {
        $this->m_value = $_value;
        $this->m_unit = $_unit;
    }

    public function getM_value() {
        return $this->m_value;
    }

    public function getM_unit() {
        return $this->m_unit;
    }

    public function setM_value($_value) {
        $this->m_value = $_value;
    }

    public function setM_unit($_unit) {
        $this->m_unit = $_unit;
    }

    public function serialize() {
        return [
            'value' => $this->m_value,
            'unit' => $this->m_unit
        ];
    }
}

