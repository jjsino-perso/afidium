<?php
namespace Tests\App\Converter;

use PHPUnit\Framework\TestCase;
use App\Converter\FacilityConverter;

class FacilityConverterTest extends TestCase {
    
    public function testExtractPool() {
        $converter = new FacilityConverter($this->setup());
        
        $this->assertTrue($converter->convert('swimmingpool'));
        $this->assertTrue($converter->convert('spa'));
        $this->assertTrue($converter->convert('parking'));
        $this->assertTrue($converter->convert('wedding'));
        $this->assertFalse($converter->convert('pmr'));
        $this->assertFalse($converter->convert('ecotourism'));
        $this->assertFalse($converter->convert('familyFriendly'));
        $this->assertTrue($converter->convert('fitness'));
        $this->assertTrue($converter->convert('golf'));
        $this->assertFalse($converter->convert('seaside'));
    }
    
    protected function setup() {
        parent::setup();
        return $str = '<hotel>
            <hotelCode>1</hotelCode>
            <name>Bellagio</name>
            <stateCode>NV</stateCode>
            <country>USA</country>
            <countryCode>US</countryCode>
            <cutoffAge>0</cutoffAge>
            <limitationPolicies>
              <![CDATA[<ul><li>Suites include one king bed and allow for one rollaway.</li><li>Maximum stay: 14 nights</li><li>Minimum check-in age is 21 years</li><li>Children are charged as adults</li><li>Certain travel dates may require a minimum length of stay.</li><li>Name/date changes are not allowed over stop sell dates</li><li>No Shows and Early Departures are subject to 100% of the booking charges</li><li>All rooms non-smoking - smoking fee of $300 will be charged to guests who break the smoking policy (fee is subject to change)</li><li>A valid credit card or debit card is required upon arrival to register and a $150/night incidental deposit will be charged to guests (subject to change).</li><li><strong>MGM Resorts Parking Terms & Conditions: </strong>All fees and policies are subject to change. An additional full-day parking fee (self or valet) will be charged for each additional day or fraction of a day over the first 24 hours (no grace period). The self-parking fees at each MGM Resorts property is NOT applicable during special event pricing. Parking fees may be higher during special events. For registered hotel guests, the 24 hour parking fee (self or valet) includes "in and out" privileges at the guest\'s originating MGM Resort property and also includes the same "in and out" parking privileges at any other MGM Resort property within the same 24 hour period, subject to availability. <strong>LOST TICKET: </strong>If a guest loses their self-parking ticket, MGM Resorts properties will attempt to determin how long the guest\'s vehicle has been parked at the facility. If the property can verify how long the guest has been parked, the guest will be charged for the actual amount of time parked. If the property cannot verify how long the guest has been parked, the guest will be subject to a lost ticket parking fee of $30 (fee subject to change).</li></ul>]]>
            </limitationPolicies>
            <description>
              <![CDATA[The hotel rooms and suites at the Bellagio offer the perfect blend of beauty and elegance.]]>
            </description>
            <facilities>
              <![CDATA[<ul><li>Air Conditioning</li><li>Hair Dryer</li><li>Iron & Ironing Board</li><li>Individual Climate Control</li><li>In-room safe</li><li>iPod docking station</li><li>Bathrobes</li><li>Desk</li><li>Cable TV</li><li>Free WiFi</li><li>Phone</li><li>A mandatory Daily Resort Fee is payable by guests directly at the hotel for both Package and Land Only (L/O) rate plans.<strong><br /></strong></li></ul><ul><li>Resort fee includes:</li></ul><ul><ul><li>Property-wide high speed internet acess (public spaces and in-room)</li><li>Unlimited local and toll free calls</li><li>Airline boarding pass printing</li><li>Notary service</li><li>Fitness center access for guests 18+</li></ul></ul><p><strong>Effective January 31, 2018:</strong></p><ul><li>Self-Parking</li><ul><li>1 to 2 hours = $9</li><li>2 to 4 hours = $15</li><li>4 to 24 hours = $18</li><li>Over 24 hours = $18 each additional day or fraction of a day over the first 24 hour period</li></ul><li>Valet Parking:</li><ul><li>0 to 2 hours = $21</li><li>2 to 4 hours = $24</li><li>4 to 24 hours = $30</li><li>Over 24 hours = $30 each additional day or fraction of a day over the first 24 hour period</li></ul></ul><p><em>*Parking fees subject to change. Vehicles that are self-parked for less than 1 hour will NOT be subject to a parking fee. For registered hotel guests, the 24 hour parking fee (self or valet) includes "in and out" privileges at the guest\'s originating MGM Resort property and also includes the same "in and out" parking privileges at any other MGM Resort property within the same 24 hour period, subject to availability.</em></p>]]>
            </facilities>
            <recreation>
              <![CDATA[<ul><li>Casino</li><li>Five outdoor pools, 4 spas and 52 private cabanas in a Mediterranean courtyard setting (seasonal)</li><li>Salon Bellagio: facials, manicures and pedicures</li><li>Spa Bellagio: massage therapies, hydrotherapy services and new unique body treatments, steam rooms, saunas, and an exercise room with Cybex equipment</li><li>"The Bank Nightclub at Bellagio"</li><li>"Hyde Bellagio"</li><li>Conservatory & Botanical Gardens</li><li>2 casual dining restaurants and 5 quick eats outlets</li><li>Lago, an Italian restaurant by Julian Serrano (opening March 2015)</li><li>Two wedding chapels and Terrace of Dreams</li><li>"O&trade;" Cirque du Soleil&reg;</li><li>The Bellagio Gallery of Fine Art</li><li>Fine Shops & Boutiques at Via Bellagio</li><li>The Fountains of Bellagio</li><li>Golf at "Shadow Creek" - One of the nation\'s most exclusive and top rated golf courses.</li><li>Executive Suite Lounge, available to guests who book Bellagio Penthouse Suites, offers:
        <ul><li>7am - 11am, Continental Breakfast - Breakfast items include pastries, muffins, juices, coffee</li><li>11am - 5pm, Afternoon Presentation - Variety of cookies, bread sticks, dried fruits, whole fruits</li><li>5pm - 10 pm, Evening Presentation - Brioche, chocolate chip cookies, chocolate covered strawberries</li><li>Wi-Fi access</li><li>Laptops available</li><li>Faxing/Printing/Copying</li><li>Boarding pass printing</li><li>Line passes to Buffet & Cafe Bellagio</li><li>Show and Restaurant reservations with preferred seating</li><li>Floral arrangements upon request</li><li>Spa/Salon appointments</li><li>Nightclub VIP table reservations</li></ul></li></ul>]]>
            </recreation>
          </hotel>';
    }
}
