<?php

require 'bootstrap.php';

use App\App;
use App\Config;

if (isset($argv[1]) && $argv[1] === 'clear') {
    $files = glob(Config::DATA_DIRECTORY . Config::DATA_FILE_PREFIX . '*');
    foreach ($files as $file) {
        if (is_file($file)) {
            unlink($file);
        }
    }
}

$app = new App();
$app->run();

