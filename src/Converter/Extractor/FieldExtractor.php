<?php

namespace App\Converter\Extractor;

use App\Exception\XMLExtractException;

class FieldExtractor {
    
    /** 
     * @var SimpleXMLElement node
     */
    protected $m_node;
    
    public function __construct(\SimpleXMLElement $node = null) {
        if ($node) {
            $this->m_node = $node;
        }
    }
    
    /**
     * @param string $_field
     * @return string data 
     */
    public function getFirstNodeData($_field) {
        $result = $this->m_node->$_field;
        if (!$result) {
            return null;
        }
        return (string) $result[0];
    }
    
    /**
     * 
     * @param string $_query
     * @return array
     * @throws XMLExtractException
     */
    public function getQueryData($_query) {
        $this->checkNodeBeforeQuery();
        
        $nodes = $this->m_node->xpath($_query);
        
        if ($nodes === FALSE) {
            throw new XMLExtractException(XMLExtractException::XPATH_QUERY_ERROR);
        }
        
        return array_map(function($i) {
            return (string)$i;
        }, $nodes);
    }
    
    public function getM_node(): \SimpleXMLElement {
        return $this->m_node;
    }

    public function setM_node(\SimpleXMLElement $_node) {
        $this->m_node = $_node;
    }
    
    private function checkNodeBeforeQuery() {
        if (!$this->m_node) {
            
        }
    }
}
