<?php

namespace App\Converter\Extractor;

abstract class CountryLanguage {

    const DEFAULT_LANG = 'EN';
    
    private static $m_list = [
        'US' => 'EN',
        'FR' => 'FR',
        'UK' => 'EN'
    ];
    
    /**
     * 
     * @param string $_countryCode
     * @return string
     */
    public static function getLanguage($_countryCode) {
        if ($_countryCode && isset(self::$m_list[$_countryCode])) {
            return self::$m_list[$_countryCode];
        }
        return self::DEFAULT_LANG;
    }
}

