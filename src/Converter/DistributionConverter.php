<?php

namespace App\Converter;

use App\Config;

class DistributionConverter extends AbstractConverter implements ConverterInterface {
    
    const FIELD_NAME = 'hotelCode';

    /**
     * @param SimpleXMLElement $node
     * @param string $_field
     * @return array data 
     */
    public function convert($_field = null) {
        $data = $this->m_extractor->getFirstNodeData(self::FIELD_NAME);
        return [Config::DISTRIBUTOR => $data];
    }
}
