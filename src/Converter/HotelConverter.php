<?php

namespace App\Converter;

use App\Model\Hotel;
use App\Model\Text\Text;
use App\Converter\FacilityConverter;
use App\Converter\MediaConverter;
use App\Converter\Extractor\CountryLanguage;

class HotelConverter {

    const FACILITY_FIELDS = 'facility';
    const DISTRIBUTION_FIELDS = 'distribution';
    const SIMPLE_FIELDS = 'fields';
    const FLOAT_FIELDS = 'float';
    const MEDIA_FIELDS = 'media';

    /**
     *
     * @var \SimpleXMLElement data
     */
    private $m_data;

    public function getM_data() {
        return $this->m_data;
    }

    public function setM_data($_data) {
        $this->m_data = $_data;
    }

    /**
     * @return Hotel
     */
    public function convert() {
        $hotel = new Hotel();
        
        $distribution = $this->createConverter(self::DISTRIBUTION_FIELDS)->convert();
        $hotel->setM_distribution($distribution);
        
        $this->convertFloatFields($hotel);
        $this->convertLanguage($hotel);
        $this->convertDescription($hotel);
        $this->convertFacilities($hotel);
        $this->convertFamilyFriendly($hotel);
        $this->convertMedia($hotel);
        
        return $hotel;
    }
    
    /**
     * 
     * @param Hotel $_hotel
     */
    private function convertFloatFields(&$_hotel) {
        $converter = $this->createConverter(self::FLOAT_FIELDS);
        $_hotel->setM_latitude($converter->convert('latitude'));
        $_hotel->setM_longitude($converter->convert('longitude'));
        $_hotel->setM_ratingLevel($converter->convert('starRating'));
    }
    /**
     * 
     * @param Hotel $_hotel
     */
    private function convertFacilities(&$_hotel) {
        $converter = $this->createConverter(self::FACILITY_FIELDS);
        $_hotel->setM_ecotourism($converter->convert('ecotourism'));
        $_hotel->setM_charm($converter->convert('charm'));
        $_hotel->setM_exceptional($converter->convert('exceptional'));
        $_hotel->setM_familyFriendly($converter->convert('familyFriendly'));
        $_hotel->setM_fitness($converter->convert('fiteness'));
        $_hotel->setM_parking($converter->convert('parking'));
        $_hotel->setM_golf($converter->convert('golf'));
        $_hotel->setM_seaside($converter->convert('seaside'));
        $_hotel->setM_pmr($converter->convert('pmr'));
        $_hotel->setM_wedding($converter->convert('wedding'));
        $_hotel->setM_swimmingpool($converter->convert('swimmingpool'));
    }
    
    /**
     * 
     * @param Hotel $_hotel
     */
    private function convertFamilyFriendly(&$_hotel) {
        $cutoffAge = (int) ($this->createConverter(self::SIMPLE_FIELDS)->convert('cutoffAge'));
        $_hotel->setM_familyFriendly(($_hotel->getM_familyFriendly() && $cutoffAge < 10));
    }
    
    /**
     * @param Hotel $_hotel
     * @return string $language
     */
    private function convertLanguage(&$_hotel) {
        $countryCode = $this->createConverter(self::SIMPLE_FIELDS)->convert('countryCode');
        $lang = CountryLanguage::getLanguage($countryCode);
        $_hotel->setM_language($lang);
        return $lang;
    }
    
    /**
     * 
     * @param Hotel $_hotel
     */
    private function convertDescription(&$_hotel) {
        $converter = $this->createConverter(self::SIMPLE_FIELDS);
        $language = $_hotel->getM_language() ? $_hotel->getM_language() : $this->convertLang($_hotel);
        $description = new Text($language, 'Description', 'Description', $converter->convert('description'));
        $_hotel->setM_introductionText($description);
    }

    /**
     * 
     * @param Hotel $_hotel
     */
    private function convertMedia(&$_hotel) {
        $converter = $this->createConverter(self::MEDIA_FIELDS);
        $_hotel->setM_introductionMedia($converter->convert());
    }

    private function createConverter($_type) {
        switch($_type) {
            case self::FACILITY_FIELDS:
                $converter = new FacilityConverter();
                break;
            case self::DISTRIBUTION_FIELDS:
                $converter = new DistributionConverter();
                break;
            case self::MEDIA_FIELDS:
                $converter = new MediaConverter();
                break;
            case self::FLOAT_FIELDS:
                $converter = new FloatFieldConverter();
                break;
            case self::SIMPLE_FIELDS:
            default:
                $converter = new FieldConverter();
                break;
        }
        $converter->getM_extractor()->setM_node($this->m_data);
        return $converter;
    }
}
