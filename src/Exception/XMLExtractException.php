<?php

namespace App\Exception;

class XMLExtractException extends \Exception {

    const NO_SOURCE = 0;
    const XPATH_QUERY_ERROR = 1;

    private static $m_messages = [
        'No source node given',
        'Xpath query failed'
    ];

    public function __construct($_code = self::NO_SOURCE_CODE) {
        $message = self::$m_messages[$_code];
        parent::__construct($message, $_code);
    }

    public function __toString() {
        return $this->message;
    }

}